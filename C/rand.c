#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define COUPON_LEN 20

void generateAndDisplayCoupon(void)
{
    int i, j;
	char coupon_code[COUPON_LEN + 1];
	
	srand((unsigned)time(NULL)*100000000);
	
	for(i = 0; i < COUPON_LEN; i++)
	{
	    j = rand()%3;
	    /*
	    j: 0->num(48-57), 1->uppercase alpahbetic(65-90), 
	    2->lowercase alphabetic(97-122).
	    */
	    
	    switch(j)
	    {
	        case 0: coupon_code[i] = rand()%10 + '0'; break;
	        case 1: coupon_code[i] = rand()%26 + 'A'; break;
	        case 2: coupon_code[i] = rand()%26 + 'a'; break;
	        default: break;
	    }
	}
	
	coupon_code[i] = '\0';
	
	printf("%s\n",coupon_code);	
	
	
	return;
    
}

void multipleGenerateCoupon(int count)
{
    int i;
    for(i = 0; i < count; i++)
    {
        generateAndDisplayCoupon();
    }
}

int main(int argc, char const *argv[])
{
     generateAndDisplayCoupon();
     generateAndDisplayCoupon(); 
     generateAndDisplayCoupon();
//   multipleGenerateCoupon(100);


	return 0;
}